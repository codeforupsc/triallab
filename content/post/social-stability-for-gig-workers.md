---
title: Social Stability for GIG workers
subtitle: without creating hurdles for Businesses
date: 2019-11-18T04:00:00+05:30
tags:
- Social Security
- Economic Development
- GS3

---
Q:- "As manufacturing could not create as many jobs required for migrating workforce, the void is filled by flexible tech-based platform jobs’. It throws up several policy challenges. Critically analyse the challenges in this context.

**GIG Economy:-**

Definition:- New Biz model where casual workers providing the services are connected to app with the consumer.

In this model, Temp-flexible jobs are common, companies hire part-time workers, freelancers, instead of full time employee. Due to large no of people willing to work part time, GIG economy becomes cheaper. It makes harder for full time worker to develop fullly because te  mp employess are often cheaper to hire. 

Traditional economy has full time worker who rarely change positions and instead focus on life time career.

Ensuring Worker Rights in GIG economy is a challenge.

Kerala minister recently argued in framing guidelines to Ensure social stability while not creating hurdles for the biz.

**Not a Rosy pic for ensuring Social stability for GIG workers. what are the challenges?**

1) Some people are happy with part-time job. they work in leisure time 

2) in some countries Uber classified worker as "Independent Contractors"

3) California law maker passed bill to ensure holiday and sick pay for GIG worker

4) As avg monthly income fell in USA. there is hard time for the worker who are solely dependent on GIG platform.

5) Several restaurant owners are threatining swiggy/zomato over the commission.

6) as the platform is providing job for the migrating workforce more. govt is not in a position to antagonize them.

7) large no of people willing to work at cheaper costs

8) They are not employees when the right of employees comes into the pic like social security, safety, compensation in case of accidents etc

Crystal clear policy framework is required for certain degree to protect the livelihoods and social stability.