---
title: Climate Emergency
subtitle: Common interest should prevail over self interest
date: 2019-11-18T00:00:00+05:30
tags:
- Clmate Change
- Env & Disaster management
- GS3

---
Q:- “To address the climate change issues, common interest should prevail over self-interest”. Elaborate the statement in the context of around 11,000 scientists issuing the warning about climate emergency.

Q:- Modifications to the present lifestyles and investments in alternate sources hold the key for addressing climate change. Discuss.

**Climate Emergency:-**

More then 11000 scientist echoed unequivocally about the climate emergency. The research is published in the BioScience journal which is peer reviewed. Scientist used datasets on various aspects to prove that the climate change is happening at much faster pace then expected.

FIrst World climate conference in 1979 under WMO (World meteorological org) was first international conferences on climate change. later on it led to the development of World climate programmee, IPCC and UNEP.

Total 3 such conferences were held till now.

What is happening:- some example:-

1. Air passenger increased drastically
2. carbon emission is increased which increases the global temp.
3. annual per capita  meat consumption increase to 45 kg:- (a) Methane released by the cattle (b) huge natural resources  is required for cattles .

India is also facing the climate change effect:- Rising temp, uncertain monsoon, high intensive rainfall, unseasonal raining, delay in monsoon, increase cyclonic storm etc

**Recommendation by scientist:-**

1. Slowdown population growth
2. improve the consumption pattern
3. modify the lifestyle
4. reduce meat consumption (Flexiterianism= reduce meat and increase fruit, vegetables and other plant based products)
5. halt deforestation
6. Halt the fossil fuel use and invest in renewable energy production method

Climate emergency is closely linked to excessive consumption. need is to reduce the ecological footprint.

**Way Forward**

Crisis could lead up to the more tragedy for the downtrodden people. 

The need of the hour is "Common interest should prevail over the self-interest".

Two imp dimension of Climate conference  to be successful:-

1) Developed countries should recognize that they are mainly responsible for climate change and accordingly they should help developing and under-developed countries by giving them:- **Liberal Financing** (GCF), **Technology transfer,** more **investment in R&D** in green technology

2) Common interest should prevail over self interest. As countries like USA, Brazil are looking inwards, it creates problems for all human existence.