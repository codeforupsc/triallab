---
title: Improving productivity of MSME
subtitle: key for Global Competitiveness
date: 2019-11-18T00:00:00+05:30
tags:
- MSME
- Economic Development
- GS3

---
Q:- Proving the productivity of MSMEs holds the key for global competitiveness. Examine various issues and suggest various measures for improving the productivity.

**MSME Classification:-**

1. on the basis of Investment. It is different for the "Service and Manufacturing"
2. Trying to classify based on the "Annual turnover"

The need of the hour is global competitiveness.

Global competitiveness will benefited by 2 ways :- 1) we can reap the benefit of FTA and 2) The product could be part of the global value chain

So one of the component in achieving global competitiveness is by Improving the MSME sector.

Most of the MSME wants to operates INFORMALLY because of :- Circumstances like:-

1. Lack of access to credit from the formal channels like BANK, NBFC etc. That's why they go to INFORMAL channels and pay higher interest rate
2. Constrains regarding Land issues in urban areas. so they try to operate from the villages
3. Constrains in Upgrading the machines due to lack of money.
4. Labour laws:- once the MSME becomes formal several labour laws comes into picture. and complying those laws incur lot of expenditure
5. If law is applied then govt interface comes into the picture.Thus an element of corruption is added to this dimension.

**How to make the MSME more Globally Competitive** :-

1. Reduce the regulatory burden:- once the no of people crossed 20. The no of regulatory burden on MSME increase drastically due to various labour laws comes into the equation. Along with the laws govt-machinery also comes into action and Element of Corruption adds up into the cost of MSME. **SOLUTION:-** Scrap overlapping and redundant laws; NO need of frequent renewal of licence; Third party certification will reduce the govt interface and element of corruption.
2. Improving the Labour Productivity:- wages are low in India but the production cost per unit becomes high due to low productivity. Vicious cycle of "Low Equilibrium" ie low productivity with low wages and it is creating vicious cycle.**Solution:-** Improve the productivity by upgrading the Skills through National skill mission. Need is to provide skill for younger people who are entering the workforce.
3. Improve productivity by introducing "CLUSTER based approach"
4. Land availability is the major constrain regarding the MSME. The need of the hour is flexible and pragmatic land use regulation. **Plug and play system** for unused and idle land in the industrial parks.
5. Technological Upgradation:- Essential for Growth and Survival both for MSME. Need of the hour is to procure in bulk and provide at the lesser interest rate to the MSME.

**Way forward:-**

1. Make them accessible to formal credit channels and loan in lesser interest rate. **Mudra loan** needs to tweak in such direction.
2. Training in skills.
3. Land availability
4. Technological Upgradation