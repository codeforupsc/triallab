---
title: Uncontrolled Online Political Advertising
subtitle: Affect "Voter Behaviour" and "Free and Fair Election"
date: 2019-11-18T03:00:00+05:30
tags:
- Election
- Constitution and Polity
- GS2

---
Q:- How Uncontrolled online political advertising is affecting the voter's behaviour and creating hurdles for free and fair elections.

> Free speech (facebook path) v/s fair election (twitter path)

Free and fair elections are constrained because of

1. Paid News
2. Ownership of media houses with political inclination
3. Electoral bonds- origin of the Donor is not known to the common man.
4. Change in political funding:- by foreign entities
5. online political advertising

These affects the voter behaviour.

Perils of political advertising can be explained by 4 terms-

Confirmation bias, Intellectual isolation, Eco-chamber Phenomenon, manufacture consent.

Confirmation bias:- cognitive bias/ psychological bias that involves **favouring information that confirms your previously existing beliefs** or biases. People do not become the rational person.

Intellectual isolation:- created with personalized searches, when a website algorithm selectively guesses what information a user would like to see based on past clicks and search history.

Eco-chamber Phenomenon:- situation, where certain ideas, beliefs or data points are reinforced through repetition

> Confirmation bias & Intellectual isolation creates Eco chamber phenomena which in turn lead to manufacture consent

manufacture consent:- citizens become willing and obedient, consenting and **unquestioning**, by way of **propaganda** through **mass media**.

Problems associated with online political advertisement:-

1. Micro-trageting:- engage in personalized targeting
2. Invisibility:- anothe feature. design the ad to target a specific group of people without knowing the others. Can lead to social unrest

Proponent argument on online political advertising:-

1. Reach all section of the society
2. newspaper also affect public opinion so social media should not only be targeted
3. Ad in the newspaper are also not transparent about who paid, whether paid news or not
4. online ad can be traceable along with Money trail
5. political ad is only propagating their manifesto and hence not be censored

Those who are running the platform should not be liable for what people are seening on the platform.

Opponent Argument:-

1. Substantial population is illiterate, poor edu standards. May affect the voter behaviour
2. strong dividing line based on caste/communities/religion are still there. May reinforce the confirmation Bias.
3. Money trail lot of times are not traceable in the online ad
4. Microtargeting got the potential for affecting the voter behaviour
5. Invisible nature of online ad can disturb the social harmony and strengthen the divisive politics as the outside world does not get to know what is being shown in the advertisement.
6. **RPA Act** clearly list certain things for free and fair election
7. Restrictions which apply to freedom of speech and expression on newspaper/press should also apply to social media as well
8. Biz model of tech companies are **subverting the democratic process.**

Conclusion:-

1. Election commission should act decisively
2. EC should look at the content of the ad and decide whether they are based on caste and communal lines
3. EC should create the infra for these aspects

In election time, EC has the mandate to take on diff dept and ensure that subversion of democratic process does not happen