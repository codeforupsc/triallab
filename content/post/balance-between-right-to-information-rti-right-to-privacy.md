---
title: Balance between Right to Information (RTI) & Right to Privacy
subtitle: Chief Justice as Public Authority & RTI can't be a tool of Surveillance
date: 2019-11-18T06:00:00+05:30
tags:
- Ethics
- GS4

---
Q:- Supreme Court recently ruled Chief Justice of India as the public Authority. What inferences can be drawn from the judgment for striking the balance between RTI and Right to Privacy?

**SC Judgement that CJI is a public authority:-**

Imp points

1. CJI is a Public Authority \[Judges are not above the Law\]
2. But when it comes to making Information public from CJI under RTI act, Two imp aspects came into the picture:-

a) Striking a balance b/w **RTI \[Art 19\]** and Right to **privacy \[Art 21\]** \[Both are Constitutional Fundamental Right but they are NOT ABSOLUTE. **Right to Proportionality** is applied ie disclosing info Should be proportional to the public interest\]

b) RTI Act should not become the tool for Surveillance. Transparency is imp but **Judicial independence** should not be curtailed on the name of transparency. Transparency can't be absolute.

What is Public Authority:-

Definition:- Any authority or body established by the Constitution, law made by parliament/legislature, notification issued, substantially financed by govt or its bodies, NGO substantially financed.

**RTI act created legal framework by defining Public authorities.It allows citizen to ask public authorities for information.**

Clarity is not there in Definition of NGO. sometimes they come under it sometimes don't. Substantially financed does not mean 50% or more.

> CBI is taken out of Public authority
>
> BCCI is brought under it.
>
> ie it is in the purview of RTI Act, what constitutes Public authority.
>
> CIC ruled that political parties are Public authorities BUT political parties are not listening.
>
> As stands today:- PM, Prsident, CJI they are the Public authority

**RTI Act**

1. Under Section 4 of it, Public authorities should SUO-MOUTO reveal the information. But they are not doing so.
2. Section 8 (1)(j) PROHIBITS sharing personal information that has no nexus with **public activity,** which **amounts to invasion of privacy** unless there is the larger public interest justifies such disclosure. \[Trying to strike balance b/w RTI and Right to privacy\]