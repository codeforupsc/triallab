---
title: Bridzing the Gap between Digital Haves & Have-Nots
subtitle: for creating Equity in society
date: 2019-11-18T04:05:00+05:30
tags:
- Social Justice & Governance
- GS2

---
Q:- Bridging the gap between digital Haves and Have-Nots becomes mandatory to ensure equity. Discuss with various Government interventions in this direction.

**Equity** means the fruit of growth process should go to all the sections of the society.

The responsibility lies with the Govt to bridge this gap.

**Why addressing the divide b/w DIGITAL Haves and Have-Nots is needed:-**

1. To ensure Equity
2. in digital era where the govt is providing services digitally there is need of reducing the digital gap b/w have and have not and Even the Gender gap in digital literacy.
3. UN articulate about internet role in "enabling the freedom of speech" and "reducing Inequality"
4. Internet as basic human right

**Govt intervention** in this direction:-

1. PMGDISHA:- empowering at least one person in every household in digital literacy by 2020. develop skills of interaction through digital world.
2. Internet Saathi:- intiative of TATA TRUST AND GOOGLE to bridge the gender gap in digital lieracy. based on **train the trainer model.** women from villages are trained and provide data-enabled devices to train other women of the village.
3. **Kerala exp:-** Kerala Fibre optical network (KFON) an ambitious project to deliver the Free internet access to BPL families. may lead to steep digital revolution

Govt has to play an interventionist role in plugging the gap. India still have **digital Have-Nots** **in millions** despite huge growth.

**Lower Internet penetration** is one big hurdle; **Paying capacity** is another reason (Kerala intervention is the right work in this direction).